#pragma once
#include <string>

class Thing
{
public:
	Thing(std::string name, int size);
	Thing();
	~Thing();

	std::string name;
	int size;
};

