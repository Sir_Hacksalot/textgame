#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Thing.h"

int main()
{
	Thing thing1("Barry", 99);
	Thing thing2("Tom", 35);
	Thing thing3("Judith", 27);

	//create a vector to hold our things
	std::vector<Thing> things;
	things.push_back(thing1);
	things.push_back(thing2);
	things.push_back(thing3);

	std::string file = "data.txt";

	std::ofstream out;
	out.open(file, std::ios::app);

	if (!out)
	{
		std::cerr << "Unable to write to the file.\n";
	}
	else
	{
		for (auto iter = things.begin(); iter < things.end(); iter++)
		{
			out << iter->name << std::endl;
			out << iter->size << std::endl;
		}
	}

	out.close();

	//read the file in
	std::ifstream in;
	in.open(file);

	if (in.good())
	{
		while (!in.eof())
		{
			std::string line;
			getline(in, line);
			if (line == "")
			std::cout << "Name: " << line << std::endl;
			getline(in, line);
			std::cout << "Size: " << line << std::endl;
		}
	}
	else
	{
		std::cerr << "I totally handled that. Not.\n";
	}

	in.close();


	system("PAUSE");
	return 0;
}


