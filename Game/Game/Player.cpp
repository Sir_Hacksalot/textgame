#include "Player.h"



Player::Player()
{
}

Player::Player(std::string name)
{
	this->name = name;
}


Player::~Player()
{
}

void Player::setName(std::string name)
{
	this->name = name;
}

void Player::setDescription(std::string description)
{
	this->description = description;
}

void Player::setHealth(int health)
{
	this->health = health;
}

void Player::setAttack(int attack)
{
	this->attack = attack;
}

void Player::setLevel(int level)
{
	this->level = level;
}

std::string Player::getName()
{
	return name;
}

std::string Player::getDescription()
{
	return description;
}

int Player::getHealth()
{
	return health;
}

int Player::getAttack()
{
	return attack;
}

int Player::getLevel()
{
	return level;
}
