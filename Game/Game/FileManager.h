#pragma once
#include <vector>
#include "Location.h"

class FileManager
{
public:
	~FileManager();

	static void save(const std::vector<Location> &locations, const char* filename);
	static void restore(std::vector<Location> &locations, const char* filename);

private:
	FileManager();
};

