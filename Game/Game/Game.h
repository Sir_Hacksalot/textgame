#pragma once
#include "Location.h"
#include <vector>
#include "Warrior.h"
#include "Thief.h"

class Game
{
public:
	Game();
	~Game();

	void run();
	

	Player player;

private:
	void init();
	void playerSelect();
	void update();
	void move();

	void changeLocation(int number);
	int getMoveInput();
	void pickUpItem();
	//bool isGameOver();

	std::vector<Location> locations;
	Location currentLocation;
	bool gameOver = false;
};

