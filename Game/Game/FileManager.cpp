#include "FileManager.h"
#include <boost\archive\binary_oarchive.hpp>
#include <boost\archive\binary_iarchive.hpp>
#include <fstream>



FileManager::FileManager()
{
}


FileManager::~FileManager()
{
}

void FileManager::save(const std::vector<Location> &locations, const char* filename)
{
	//make an archive
	std::ofstream outputFileStream(filename);
	boost::archive::binary_oarchive outputArchive(outputFileStream);
	outputArchive << locations;
}

void FileManager::restore(std::vector<Location> &locations, const char* filename)
{
	std::ifstream inputFileStream(filename);
	boost::archive::binary_iarchive inputArchive(inputFileStream);
	//restore the file
	inputArchive >> locations;
}
