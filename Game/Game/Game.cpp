#include "Game.h"
#include <string>
#include <iostream>
#include <climits>
#include "FileManager.h"
#include <boost\algorithm\string.hpp>

Game::Game()
{
	init();
}

Game::~Game()
{
	//gameover
}

void Game::run()
{
	while (!gameOver) 
	{
		update();
	}
	
}

void Game::update()
{
	//get info about the location
	currentLocation.getInformation();
	//pickup item
	pickUpItem();
	//move
	move();

	gameOver = false;
}

void Game::move()
{
	//get player input
	//see if it matches our directions
	//look up the value
	//change the location to the new value
	changeLocation(getMoveInput());
}

void Game::changeLocation(int number)
{
	for (auto iter = locations.begin(); iter != locations.end(); 
		iter++)
	{
		if (iter->locationNumber == number)
		{
			currentLocation = *iter;
			break;
		}
	}
}

int Game::getMoveInput()
{
	bool done = false;
	int nextLocation;

	while (!done)
	{
		//get the direction
		std::string dir;
		std::cout << "Which direction do you take? ";
		std::cin >> dir;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), 
			'\n');
		//convert to lower so we can compare strings
		boost::to_lower(dir);
		//see if the input is a valid direction
		if (dir == "north" || dir == "east" || dir == "south" ||
			dir == "west")
		{
			//the input is valid, but can the player go that way?
			int value = currentLocation.exits.find(dir)->second;
			if (value != 0)
			{
				nextLocation = value;
				done = true;
			}
			else
			{
				std::cout << "You cannot go that way.\n";
			}
		}
	}
	return nextLocation;
}

void Game::pickUpItem()
{
	std::string item = currentLocation.item;
	if (item != "none")
	{
		std::cout << "You find a " << item << "\n";
		std::cout << "you pick it up and put it in your backpack.\n\n";
		currentLocation.item = "none";
		player.inventory.push_back(item);
	}
}

void Game::init()
{
	//load stuff
	FileManager::restore(locations, "locations.dat");
	//load the current location
	changeLocation(1);
	//get player details
	playerSelect();
}

void Game::playerSelect()
{
	//this is where we will select our player type
	//get the name
	std::string name;
	std::cout << "What is your name, brave hero?\n>>";
	std::cin >> name;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	bool done = false;

	while (!done)
	{
		//get the player type
		std::string playerType;
		std::cout << "What player type do you wish to be? \n";
		std::cout << "1. Warrior\n2. Thief\n\n>>";
		std::cin >> playerType;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		
		if (playerType == "warrior")
		{
			//create a warrior
			player = Warrior(name);
			std::cout << player.getDescription() << std::endl;
			done = true;
		}
		else if (playerType == "thief")
		{
			//create a thief
			player = Thief(name);
			std::cout << player.getDescription() << std::endl;
			done = true;
		}
		else
		{
			//no idea what the user did. Try again.
		}
	}
	

}
